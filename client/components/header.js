import React, { Component } from 'react';

export default class Header extends Component {
  render(){
    return(
      <nav className="navbar navbar-dark bg-inverse">
        <div className="container">
          <div className="nav navbar-nav">
            <a className="nav-item nav-link active" href="#">Home <span className="sr-only">(current)</span></a>
            <a className="nav-item nav-link" href="#">Features</a>
            <a className="nav-item nav-link" href="#">Pricing</a>
            <a className="nav-item nav-link" href="#">About</a>
          </div>
        </div>
      </nav>
    );
  }
}
