import React, { Component } from 'react';
import { Link } from 'react-router';

export default class Intro extends Component {
  render() {
    return (
      <p className="flexed-item">
        This is a basic webpack setup for Babel 6, React 15, Redux, Bootstrap 4, Stylus and Hot Reload. Setup, install and get coding...
      </p>
    );
  }
}
