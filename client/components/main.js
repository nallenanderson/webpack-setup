import React, { Component } from 'react';
import { Link } from 'react-router';

import Header from './header';

export default class Main extends Component {
  render() {
    return (
      <div>
        <Header />
        <div id="main" className="container">
          <div className="flexed-item">
            <h1 className="display-1">Webpacking 1.0</h1>
          </div>
          {React.cloneElement(this.props.children, { ...this.props } )}
        </div>
      </div>
    )
  }
}
