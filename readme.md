# Webpacking 1.0

Basic Webpack configuration for Babel 6, React, Bootstrap, Stylus and Hot Reloading.

## Running

First `npm install` to grab all the necessary dependencies. 

Then run `npm start` and open <localhost:4000> in your browser.

## Production Build

Run `npm build` to create a dist folder and a bundle.js file.